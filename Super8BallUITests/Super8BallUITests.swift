//
//  Super8BallUITests.swift
//  Super8BallUITests
//
//  Created by bartekzabicki on 05.08.2015.
//  Copyright © 2015 bartekzabicki. All rights reserved.
//

import XCTest

class Super8BallUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        XCUIApplication().launch()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testViews() {
//        for var i=0;i<20;i+=1{
        
            let app = XCUIApplication()
            app.buttons["Info"].tap()
            app.navigationBars["Information Panel"].childrenMatchingType(.Button).matchingIdentifier("Back").elementBoundByIndex(0).tap()
            app.buttons["Admin"].tap()
            app.navigationBars["Admin Panel"].childrenMatchingType(.Button).matchingIdentifier("Back").elementBoundByIndex(0).tap()
            
//        }
    }
    func testScroll(){
        
        let app = XCUIApplication()
        let adminButton = app.buttons["Admin"]
        let backButton = app.navigationBars["Admin Panel"].childrenMatchingType(.Button).matchingIdentifier("Back").elementBoundByIndex(0)
        let tablesQuery = app.tables
        adminButton.tap()
        tablesQuery.staticTexts["You may realy on it"].swipeUp()
        tablesQuery.staticTexts["You may realy on it"].swipeDown()
        backButton.tap()
        
    }
}
