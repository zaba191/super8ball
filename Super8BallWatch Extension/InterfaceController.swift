//
//  InterfaceController.swift
//  Super8BallWatch Extension
//
//  Created by bartekzabicki on 10.08.2015.
//  Copyright © 2015 bartekzabicki. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    @IBOutlet var answerLabel: WKInterfaceLabel!
    let session : WCSession!

    override init() {
        if(WCSession.isSupported()) {
            session =  WCSession.defaultSession()
        } else {
            session = nil
        }
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
    }

    override func willActivate() {
        super.willActivate()
        
        if(WCSession.isSupported()) {
            session.delegate = self
            session.activateSession()
        }
        self.answerLabel.setText("Ask me")
        
    }

    override func didDeactivate() {
        super.didDeactivate()
    }
    
    @IBAction func refreshButton() {
        if(WCSession.isSupported()) {
            
            // create a message dictionary to send
            let message = ["answer" : "requestingString"]
            
            session.sendMessage(message, replyHandler: { (content:[String : AnyObject]) -> Void in
                }, errorHandler: {  (error ) -> Void in
                    //print("We got an error from our paired device : " + error.domain)
            })
        }
    }
    
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
        //print(message["answer"])
        if let answer = message["answer"] as! String? {
            if let answerLevel = message["answerLevel"] as! Int? {
            
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.updateAnswer(answer)
                    self.changeColor(answerLevel)
                })
            }
        }
    }


    private func updateAnswer(answer: String) {
        answerLabel.setText(answer)
    }
    
    private func changeColor(level: Int){
        switch(level){
        case 0:
            answerLabel.setTextColor(UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1))
            break
        case 1:
            answerLabel.setTextColor(UIColor(red: 52/255, green: 170/255, blue: 220/255, alpha: 1))
            break
        case 2:
            answerLabel.setTextColor(UIColor(red: 255/255, green: 45/255, blue: 85/255, alpha: 1))
            break
        default:
            print("Wrong level you're cheating!")
        }

    }
}
