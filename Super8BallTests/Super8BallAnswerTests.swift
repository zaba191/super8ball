//
//  Super8BallAnswerTests.swift
//  Super8Ball
//
//  Created by bartekzabicki on 19.08.2015.
//  Copyright © 2015 bartekzabicki. All rights reserved.
//
import CloudKit
import XCTest

class Super8BallAnswerTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testAnswerModel() {
        let tText = "test text"
        let tLevel = 2
        let tId = "Default"
        let testObject = Answer(answerText: tText, answerLevel: tLevel, answerId: CKRecordID(recordName: tId))
        XCTAssertNotNil(testObject)
        XCTAssertEqual(tText, testObject.text)
        XCTAssertEqual(tLevel, testObject.level)
        XCTAssertEqual(tId, testObject.id.recordName)
    }

}
