//
//  AdminController.swift
//  Super8Ball
//
//  Created by bartekzabicki on 06.08.2015.
//  Copyright © 2015 bartekzabicki. All rights reserved.
//

import UIKit
import CloudKit
//import Realm

class AdminController: UIViewController, UITableViewDelegate, UITableViewDataSource,UIPickerViewDataSource,UIPickerViewDelegate, UITextViewDelegate, UIGestureRecognizerDelegate, UITextFieldDelegate {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var pickerView: UIPickerView!
    @IBOutlet var adminView: AdminView!
    @IBOutlet var bgView: SmallBackgroundView!
    
    var positiveArray:[Answer] = []
    var neutralArray:[Answer] = []
    var negativeArray:[Answer] = []
    var fullArray:[Answer] = []
    var pickerData = ["Positive", "Neutral", "Negative"]
    var pickerRow:Int = 0
    var tableRow:Int!
    var oldTableRow:Int!
    var answerState:Int!
    var isButtonToMod:Int = 0
    var keyboardVisable = false
    var saveTrigger = false
    
    let defaults = NSUserDefaults.standardUserDefaults()
    let container = CKContainer.defaultContainer()
    var publicDatabase: CKDatabase?
    var currentRecord: CKRecord?
    
    override func viewWillAppear(animated: Bool) {
        getLastUpdate()
    }
    override func viewDidLoad() {
        let backgrounda = CAGradientLayer().violetColor()
        backgrounda.frame = self.adminView.frame
        self.adminView.layer.insertSublayer(backgrounda, atIndex: 0)
        super.viewDidLoad()
        self.navigationController?.navigationItem.rightBarButtonItem = self.navigationController?.navigationBar.topItem?.rightBarButtonItem
        adminView.addAnswerButton.setTitle("Add new answer", forState: UIControlState.Normal)
        adminView.textField.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        pickerView.dataSource = self
        pickerView.delegate = self
        adminView.activityIndicator.startAnimating()
        publicDatabase = container.publicCloudDatabase
        
        downloadAnswers { (fullArray) -> Void in
            self.fullArray=fullArray
            self.refreshTable()
            self.adminView.activityIndicator.stopAnimating()
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        adminView.textField.resignFirstResponder()
        return true
    }

    func downloadAnswers(completion: (([Answer]) -> Void)){
        let predicate = NSPredicate(value: true)
        
        let query = CKQuery(recordType: "Answers", predicate: predicate)
        
        publicDatabase?.performQuery(query, inZoneWithID: nil,
            completionHandler: ({results, error in
                
                if (error != nil) {
                    dispatch_async(dispatch_get_main_queue()) {
                    }
                } else {
                    if results!.count > 0 {
                        for record in results! {
                            self.currentRecord = record
                            
                            dispatch_async(dispatch_get_main_queue()) {
                                let aLevelResult = record.objectForKey("answerLevel") as! Int
                                let answerResult = record.objectForKey("answer") as! String
                                let answerId = record.recordID
                                let answer = Answer(answerText: answerResult, answerLevel: aLevelResult, answerId: answerId)
                                switch(aLevelResult){
                                case 0:
                                    let tryIndex = self.containAnswer(self.positiveArray, answer: answer)
                                    if tryIndex != -1{
                                        self.positiveArray[tryIndex] = answer
                                    }
                                    else{
                                        self.positiveArray.append(answer)
                                        self.saveTrigger = true
                                    }
                                    break
                                case 1:
                                    let tryIndex = self.containAnswer(self.neutralArray, answer: answer)
                                    if tryIndex != -1{
                                        self.neutralArray[tryIndex] = answer
                                    }
                                    else{
                                        self.neutralArray.append(answer)
                                        self.saveTrigger = true
                                    }
                                    break
                                case 2:
                                    let tryIndex = self.containAnswer(self.negativeArray, answer: answer)
                                    if tryIndex != -1{
                                        self.negativeArray[tryIndex] = answer
                                    }
                                    else{
                                        self.negativeArray.append(answer)
                                        self.saveTrigger = true
                                    }
                                    break
                                default:
                                    print("bad answerLevel")
                                }
                            }
                        }
                        dispatch_async(dispatch_get_main_queue()) {
                            if self.saveTrigger == true{
                                self.saveNew()
                            }
                            self.saveTrigger = false
                            completion(self.positiveArray+self.neutralArray + self.negativeArray)
                        }
                        
                    } else {
                        dispatch_async(dispatch_get_main_queue()) {
                            self.notifyUser("No Match Found",
                                message: "No record matching the address was found")
                        }
                    }
                }
            }))
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fullArray.count
    }

    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("AdminCell", forIndexPath: indexPath) as! AdminCell
        cell.answerLabel.text = fullArray[indexPath.row].text
        if indexPath.row < positiveArray.count{
            //Positive answer
            cell.answerLevel.textColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
            cell.answerLevel.text = "Positive"
        }
        else if indexPath.row >= positiveArray.count + neutralArray.count{
            //negative answer
            cell.answerLevel.textColor = UIColor(red: 255/255, green: 45/255, blue: 85/255, alpha: 1)
            cell.answerLevel.text = "Negative"
        }
        else{
            //neutral answer
            cell.answerLevel.textColor = UIColor(red: 52/255, green: 170/255, blue: 220/255, alpha: 1)
            cell.answerLevel.text = "Neutral"
        }
        cell.deleteCellButton.row = indexPath.row
        cell.deleteCellButton.addTarget(self, action: "deleteAnswer:", forControlEvents: UIControlEvents.TouchUpInside)
        cell.backgroundColor = UIColor.clearColor()
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.darkGrayColor()
        cell.selectedBackgroundView = bgColorView
        return cell
    }
    func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        self.tableView.reloadData()
        if keyboardVisable == true{
            self.view.endEditing(true)
            keyboardVisable = false
        }
        if fullArray[indexPath.row].id.recordName != "Default"{
            if oldTableRow != nil{
                tableRow = indexPath.row
                if oldTableRow != tableRow{
                    //Modify
                    editAnswer(tableRow)
                }
                else{
                    //add
                    if isButtonToMod == 1{
                        pickerView.userInteractionEnabled = true
                        adminView.addAnswerButton.setTitle("Add new answer", forState: UIControlState.Normal)
                        isButtonToMod = 0
                    }
                    else{
                        //first tap
                        editAnswer(tableRow)
                    }
                }
            }
            else{
                tableRow = indexPath.row
                editAnswer(tableRow)
            }
        }
        oldTableRow = tableRow
        return indexPath
    }
    
    //MARK: Data Sources
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        self.pickerRow = row
        return pickerData[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.pickerRow = row
    }

    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: pickerData[row], attributes: [NSForegroundColorAttributeName : UIColor.lightGrayColor()])
        return attributedString
    }
    
    override func viewDidAppear(animated: Bool) {
        self.tableView.reloadData()
    }
    
    @IBAction func addNewAnswer(sender: UIButton) {
        //print("\(adminView.textField.text!) \(pickerRow)")
        
        if(adminView.textField.text! != ""){
            switch(isButtonToMod){
            case 0:
                addAnswer()
                
                break
            case 1:
                modifyAnswer()
                
                break
            default:
                print("Bad flag to isButtonToMod")
            }
            if oldTableRow != nil{
                if oldTableRow == tableRow{
                    adminView.addAnswerButton.setTitle("Add new answer", forState: UIControlState.Normal)
                    isButtonToMod = 0
                }
            }
        }
        saveNew()
        adminView.textField.text = ""
    }
    @IBAction func deleteAnswer(sender: DeleteCellButton) {
        if fullArray[sender.row].id.recordName == "Default"{
            return
        }
        if sender.row < positiveArray.count{
            positiveArray.removeAtIndex(sender.row)
        }
        else if sender.row >= positiveArray.count + neutralArray.count{
            negativeArray.removeAtIndex(sender.row-(positiveArray.count + neutralArray.count))
        }
        else{
            neutralArray.removeAtIndex(sender.row-positiveArray.count)
        }
        self.publicDatabase!.deleteRecordWithID(fullArray[sender.row].id, completionHandler: {recordID, error in
        })
        fullArray.removeAtIndex(sender.row)
        refreshTable()
        pickerView.userInteractionEnabled = true
        adminView.addAnswerButton.setTitle("Add new answer", forState: UIControlState.Normal)
        isButtonToMod = 0
        if keyboardVisable == true{
            self.view.endEditing(true)
            keyboardVisable = false
        }
        saveNew()
    }
    
    func editAnswer(row:Int){
        if row < positiveArray.count{
            answerState = 0
        }
        else if row < (positiveArray.count + neutralArray.count){
            answerState = 1
        }
        else{
            answerState = 2
        }
        adminView.textField.text = fullArray[row].text
        adminView.addAnswerButton.setTitle("Change this answer", forState: UIControlState.Normal)
        adminView.addAnswerButton.titleLabel?.adjustsFontSizeToFitWidth = true
        isButtonToMod = 1
        self.pickerView.selectRow(fullArray[row].level, inComponent: 0, animated: true)
        self.pickerView.userInteractionEnabled = false

    }
    
    func notifyUser(title: String, message: String) -> Void
    {
        let alert = UIAlertController(title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.Alert)
        
        let cancelAction = UIAlertAction(title: "OK",
            style: .Cancel, handler: nil)
        
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true,
            completion: nil)
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        adminView.textField.endEditing(true)
    }
    
    func modifyAnswer(){
        fullArray[tableRow].text = adminView.textField.text!
        switch(answerState){
        case 0:
            //good
            positiveArray[tableRow].text = adminView.textField.text!
            break
        case 1:
            //neutral
            neutralArray[tableRow-positiveArray.count].text = adminView.textField.text!
            break
        case 2:
            //negative
            negativeArray[tableRow - (positiveArray.count + neutralArray.count)].text = adminView.textField.text!
            break
        default:
            print("Bad row")
        }
        self.publicDatabase!.fetchRecordWithID(fullArray[tableRow].id) { (updatedRecord, error) -> Void in
            if error != nil {
                dispatch_async(dispatch_get_main_queue(),{
                    dispatch_async(dispatch_get_main_queue()) {
                        self.notifyUser("Save Error", message: error!.localizedDescription)
                    }
                })
                return
            }
            
            updatedRecord!.setObject(self.fullArray[self.tableRow].text, forKey: "answer")
            self.publicDatabase?.saveRecord(updatedRecord!, completionHandler: { (savedRecord, error) -> Void in
            })
        
        }
        pickerView.userInteractionEnabled = true
        refreshTable()
    }
    
    func addAnswer(){
        //print("Dodaje nowe")
        let myRecord = CKRecord(recordType: "Answers")
        myRecord.setObject(adminView.textField.text!, forKey: "answer")
        myRecord.setObject(pickerRow, forKey: "answerLevel")
        let myAnswer = Answer(answerText: adminView.textField.text!, answerLevel: pickerRow, answerId: myRecord.recordID)
        switch(pickerRow){
        case 0:
            //good
            positiveArray.append(myAnswer)
            fullArray.insert(myAnswer, atIndex: positiveArray.count-1)
            break
        case 1:
            //neutral
            neutralArray.append(myAnswer)
            fullArray.insert(myAnswer, atIndex: positiveArray.count + neutralArray.count-1)
            break
        case 2:
            //negative
            negativeArray.append(myAnswer)
            fullArray.insert(myAnswer, atIndex: fullArray.count-1)
            break
        default:
            print("Bad row")
        }
        
        
        
        publicDatabase!.saveRecord(myRecord, completionHandler:
            ({returnRecord, error in
                if let err = error {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.notifyUser("Save Error", message: err.localizedDescription)
                    }
                }
            }))
        adminView.textField.text! = ""
        refreshTable()
    }
    
    func refreshTable(){
        self.tableView!.reloadData()
    }

    func getLastUpdate(){
        if let data = defaults.objectForKey("positiveArray") as? NSData {
            let unarc = NSKeyedUnarchiver(forReadingWithData: data)
            self.positiveArray = unarc.decodeObjectForKey("root") as! [Answer]
        }
        if let data = defaults.objectForKey("neutralArray") as? NSData {
            let unarc = NSKeyedUnarchiver(forReadingWithData: data)
            self.neutralArray = unarc.decodeObjectForKey("root") as! [Answer]
        }
        if let data = defaults.objectForKey("negativeArray") as? NSData {
            let unarc = NSKeyedUnarchiver(forReadingWithData: data)
            self.negativeArray = unarc.decodeObjectForKey("root") as! [Answer]
        }
        self.fullArray = self.positiveArray + self.neutralArray + self.negativeArray
    }
    
    func containAnswer(tab:[Answer], answer:Answer) -> Int{
        for element in tab{
            if element.text == answer.text{
                return tab.indexOf(element)!
            }
        }
        return -1
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        keyboardVisable = true
        animateViewMoving(true, moveValue: 200)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        keyboardVisable = false
        animateViewMoving(false, moveValue: 200)
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.adminView.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }
    
    func saveNew(){
        defaults.setObject(NSKeyedArchiver.archivedDataWithRootObject(self.positiveArray), forKey: "positiveArray")
        
        defaults.setObject(NSKeyedArchiver.archivedDataWithRootObject(self.neutralArray), forKey: "neutralArray")
        
        defaults.setObject(NSKeyedArchiver.archivedDataWithRootObject(self.negativeArray), forKey: "negativeArray")
    }
}
