//
//  AnswerModel.swift
//  Super8Ball
//
//  Created by bartekzabicki on 06.08.2015.
//  Copyright © 2015 bartekzabicki. All rights reserved.
//

import UIKit
import CloudKit

public class Answer: NSObject, NSCoding {

    var text:String!
    var level:Int!
    var id:CKRecordID!
    
    init(answerText:String, answerLevel:Int, answerId:CKRecordID){
        self.text = answerText
        self.level = answerLevel
        self.id = answerId
    }
    
    func setMyText(text:String){
        self.text = text
    }
    required convenience public init(coder aDecoder: NSCoder) {
        let text = aDecoder.decodeObjectForKey("text") as! String
        let level = aDecoder.decodeIntegerForKey("level")
        let id = aDecoder.decodeObjectForKey("id") as! CKRecordID
        self.init(answerText: text, answerLevel: level, answerId: id)
    }
    
    public func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(id, forKey: "id")
        aCoder.encodeObject(text, forKey: "text")
        aCoder.encodeInteger(level, forKey: "level")
    }
}