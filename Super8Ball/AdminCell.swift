//
//  AdminCell.swift
//  Super8Ball
//
//  Created by bartekzabicki on 06.08.2015.
//  Copyright © 2015 bartekzabicki. All rights reserved.
//

import UIKit

class AdminCell: UITableViewCell {
    @IBOutlet var answerLabel: UILabel!
    @IBOutlet var answerLevel: UILabel!
    @IBOutlet var deleteCellButton: DeleteCellButton!

    
}
