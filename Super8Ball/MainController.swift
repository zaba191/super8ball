//
//  ViewController.swift
//  Super8Ball
//
//  Created by bartekzabicki on 05.08.2015.
//  Copyright © 2015 bartekzabicki. All rights reserved.
//

import UIKit
import CloudKit
import MobileCoreServices
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate {
    @IBOutlet var mainView: MainView!
    
    let defaults = NSUserDefaults.standardUserDefaults()
    let container = CKContainer.defaultContainer()
    var publicDatabase: CKDatabase?
    var currentRecord: CKRecord?
    let session: WCSession!
    
    var positiveArray:[Answer] = []
    var neutralArray:[Answer] = []
    var negativeArray:[Answer] = []
    var fullArray:[Answer]!
    var saveTrigger:Bool = false
    
//######################REALM#######################//
//    var realmPositiveAnswers:RLMResults
//    var realmNeutralAnswers: RLMResults
//    var realmNegativeAnswers: RLMResults
    
    required init?(coder aDecoder: NSCoder) {
        self.session = WCSession.defaultSession()
        super.init(coder: aDecoder)
    }
    
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
        if let _ = message["answer"] as? String {
            dispatch_async(dispatch_get_main_queue()) {
                if self.fullArray != nil {
                    self.mainView.activityIndicator.startAnimating()
                    self.setAnswer(self.getRandomAnswer())
                    self.mainView.activityIndicator.stopAnimating()
                }
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        if defaults.objectForKey("positiveArray") == nil && defaults.objectForKey("neutralArray") == nil && defaults.objectForKey("negativeArray") == nil {
            saveDefaults()
            //print("Saving Defaults")
        }
        else{
            getLastUpdate()
            //print("Getting last update")
        }
        downloadAnswers { (fullArray) -> Void in
            self.fullArray=fullArray
        }
    }
    override func viewDidLoad() {
        let background = CAGradientLayer().violetColor()
        background.frame = self.mainView.frame
        background.frame.size.width *= 2
        self.mainView.layer.insertSublayer(background, atIndex: 0)
        
        super.viewDidLoad()
        publicDatabase = container.publicCloudDatabase
        if(WCSession.isSupported()) {
            session.delegate = self
            session.activateSession()
        }
    }
    
    func downloadAnswers(completion: (([Answer]) -> Void)){
        let predicate = NSPredicate(value: true)
        
        let query = CKQuery(recordType: "Answers", predicate: predicate)
        
        publicDatabase?.performQuery(query, inZoneWithID: nil,
            completionHandler: ({results, error in
                
                if (error != nil) {
                    dispatch_async(dispatch_get_main_queue()) {
                    }
                } else {
                    if results!.count > 0 {
                        for record in results! {
                            self.currentRecord = record
                            
                            dispatch_async(dispatch_get_main_queue()) {
                                let aLevelResult = record.objectForKey("answerLevel") as! Int
                                let answerResult = record.objectForKey("answer") as! String
                                let answerId = record.recordID
                                let answer = Answer(answerText: answerResult, answerLevel: aLevelResult, answerId: answerId)
                                switch(aLevelResult){
                                case 0:
                                    let tryIndex = self.containAnswer(self.positiveArray, answer: answer)
                                    if tryIndex != -1{
                                        self.positiveArray[tryIndex] = answer
                                    }
                                    else{
                                        self.positiveArray.append(answer)
                                        self.saveTrigger = true
                                    }
                                    break
                                case 1:
                                    let tryIndex = self.containAnswer(self.neutralArray, answer: answer)
                                    if tryIndex != -1{
                                        self.neutralArray[tryIndex] = answer
                                    }
                                    else{
                                        self.neutralArray.append(answer)
                                        self.saveTrigger = true
                                    }
                                    break
                                case 2:
                                    let tryIndex = self.containAnswer(self.negativeArray, answer: answer)
                                    if tryIndex != -1{
                                        self.negativeArray[tryIndex] = answer
                                    }
                                    else{
                                        self.negativeArray.append(answer)
                                        self.saveTrigger = true
                                    }
                                    break
                                default:
                                    print("bad answerLevel")
                                }
                            }
                        }
                        dispatch_async(dispatch_get_main_queue()) {
                            if self.saveTrigger == true{
                                self.saveNew()
                            }
                            self.saveTrigger = false
                            completion(self.positiveArray+self.neutralArray + self.negativeArray)
                        }
                        
                    } else {
                        dispatch_async(dispatch_get_main_queue()) {
                            self.notifyUser("No Match Found",
                                message: "No record matching the address was found")
                        }
                    }
                }
            }))
    }

    
    

    func getRandomNumber(count:Int) -> Int{ //Array count
        var randomNumber:Int!
        randomNumber = Int(arc4random_uniform(UInt32(count)))
        return randomNumber
    }
    
    func getRandomAnswer() -> Answer {
        return fullArray[getRandomNumber(fullArray.count)]
    }
    
    func setAnswer(answer:Answer){
        let message = ["answer" : answer.text,"answerLevel" : answer.level]
        if (WCSession.defaultSession().paired){
            session.sendMessage(message as! [String : AnyObject], replyHandler: { (content:[String : AnyObject]) -> Void in
                }, errorHandler: {  (error ) -> Void in
                    //print("We got an error from our watch device : " + error.domain)
            })
        }
        
        mainView.mainLabel.text = answer.text
        let index = getIdByStringAndInt(fullArray,item: answer.text, level: answer.level)
        if index < positiveArray.count{
            //Positive answer
            mainView.mainLabel.textColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        }
        else if index >= positiveArray.count + neutralArray.count{
            //negative answer
            mainView.mainLabel.textColor = UIColor(red: 255/255, green: 45/255, blue: 85/255, alpha: 1)
        }
        else{
            //neutral answer
            mainView.mainLabel.textColor = UIColor(red: 52/255, green: 170/255, blue: 220/255, alpha: 1)
        }
    }
    override func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    override func motionEnded(motion: UIEventSubtype, withEvent event: UIEvent?) {
        if motion == .MotionShake{
            if fullArray != nil {
                mainView.mainLabel.fastFadeOut()
                mainView.adminButton.slowFadeOut()
                mainView.infoButton.slowFadeOut()
                mainView.activityIndicator.startAnimating()
                self.setAnswer(self.getRandomAnswer())
                mainView.activityIndicator.stopAnimating()
                mainView.mainLabel.fastFadeIn()
            }
        }
    }
    
    func notifyUser(title: String, message: String) -> Void
    {
        let alert = UIAlertController(title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.Alert)
        
        let cancelAction = UIAlertAction(title: "OK",
            style: .Cancel, handler: nil)
        
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true,
            completion: nil)
    }
    
    func getIdByStringAndInt(array:[Answer],item:String, level:Int) -> Int{
        for var i=0;i<array.count;i+=1{
            if array[i].text == item && array[i].level == level{
                return i
            }
        }
        //print("getIdByString failed")
        return 0
    }
    @IBAction func showButtons(sender: UITapGestureRecognizer) {
        mainView.adminButton.slowFadeIn()
        //mainView.adminButton.hidden = false
        mainView.infoButton.slowFadeIn()// = false
    }
 
//######################REALM#######################//
        //Check if realm db is empty
//        getLastUpdate()
//        if realmAnswers.count > 0 {
//            //Already sth inside
//            getLastUpdated()
//        }
//        else{
//            //first booting
//            saveDefaults()
//        }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        mainView.mainLabel.text = "Shake it to get answer"
        mainView.mainLabel.textColor = UIColor.lightGrayColor()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func saveDefaults(){
//######################REALM#######################//
//        let realm = RLMRealm.defaultRealm()
//        realm.beginWriteTransaction()
        
        let positiveArrayText = [
            "It is certain",
            "It is decidedly so",
            "Without a doubt",
            "Yes definitely",
            "You may rely on it",
            "As I see it, yes",
            "Most likely",
            "Outlook good",
            "Yes",
            "Signs point to yes"
        ]
        let neutralArrayText = [
            "Reply hazy try again",
            "Ask again later",
            "Better not thell you know",
            "Cannot predict now",
            "Concentrate and ask again"
        ]
        let negativeArrayText = [
            "Don't count on it",
            "My reply is no",
            "My sources say no",
            "Outlook not so good",
            "Very doubtful",
            "No"
        ]
        var positiveObjects = [Answer]()
        var neutralObjects = [Answer]()
        var negativeObjects = [Answer]()
        
        for pos in positiveArrayText {
            positiveObjects.append(Answer(answerText: pos, answerLevel: 0, answerId: CKRecordID(recordName: "Default")))
        }
        defaults.setObject(NSKeyedArchiver.archivedDataWithRootObject(positiveObjects), forKey: "positiveArray")
        
        for neu in neutralArrayText {
            neutralObjects.append(Answer(answerText: neu, answerLevel: 1, answerId: CKRecordID(recordName: "Default")))
        }
        defaults.setObject(NSKeyedArchiver.archivedDataWithRootObject(neutralObjects), forKey: "neutralArray")
        
        for neg in negativeArrayText {
            negativeObjects.append(Answer(answerText: neg, answerLevel: 2, answerId: CKRecordID(recordName: "Default")))
        }
        defaults.setObject(NSKeyedArchiver.archivedDataWithRootObject(negativeObjects), forKey: "negativeArray")

//######################REALM#######################//
//        for positiveAnswer in positiveArray {
//            var newAnswer = RealmAnswer()
//            newAnswer.answer = Answer(answerText: positiveAnswer, answerLevel: 0, answerId: CKRecordID(recordName: "Default"))
//            realm.addObject(newAnswer)
//        }
//        for neutralAnswer in neutralArray {
//            var newAnswer = RealmAnswer()
//            newAnswer.answer = Answer(answerText: neutralAnswer, answerLevel: 1, answerId: CKRecordID(recordName: "Default"))
//            realm.addObject(newAnswer)
//        }
//        for negativeAnswer in negativeArray {
//            var newAnswer = RealmAnswer()
//            newAnswer.answer = Answer(answerText: negativeAnswer, answerLevel: 2, answerId: CKRecordID(recordName: "Default"))
//            realm.addObject(newAnswer)
//        }
//        realm.commitWriteTransaction()
    }
    func getLastUpdate(){
        if let data = defaults.objectForKey("positiveArray") as? NSData {
            let unarc = NSKeyedUnarchiver(forReadingWithData: data)
            self.positiveArray = unarc.decodeObjectForKey("root") as! [Answer]
        }
        if let data = defaults.objectForKey("neutralArray") as? NSData {
            let unarc = NSKeyedUnarchiver(forReadingWithData: data)
            self.neutralArray = unarc.decodeObjectForKey("root") as! [Answer]
        }
        if let data = defaults.objectForKey("negativeArray") as? NSData {
            let unarc = NSKeyedUnarchiver(forReadingWithData: data)
            self.negativeArray = unarc.decodeObjectForKey("root") as! [Answer]
        }
        self.fullArray = self.positiveArray + self.neutralArray + self.negativeArray

//######################REALM#######################//
//        realmPositiveAnswers {
//            get {
//                let predicate = NSPredicate(format: "answer.level == 0", argumentArray: nil)
//                return RealmAnswer.objectsWithPredicate(predicate)
//            }
//        }
//        realmNeutralAnswers {
//            get {
//                let predicate = NSPredicate(format: "answer.level == 1", argumentArray: nil)
//                return RealmAnswer.objectsWithPredicate(predicate)
//            }
//        }
//        realmNegativeAnswers {
//            get {
//                let predicate = NSPredicate(format: "answer.level == 2", argumentArray: nil)
//                return RealmAnswer.objectsWithPredicate(predicate)
//            }
//        }
    }
    func containAnswer(tab:[Answer], answer:Answer) -> Int{
        for element in tab{
            if element.text == answer.text{
                return tab.indexOf(element)!
            }
        }
        return -1
    }
    func saveNew(){
        defaults.setObject(NSKeyedArchiver.archivedDataWithRootObject(self.positiveArray), forKey: "positiveArray")
        
        defaults.setObject(NSKeyedArchiver.archivedDataWithRootObject(self.neutralArray), forKey: "neutralArray")
        
        defaults.setObject(NSKeyedArchiver.archivedDataWithRootObject(self.negativeArray), forKey: "negativeArray")
    }
}

