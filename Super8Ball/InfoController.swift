//
//  InfoController.swift
//  Super8Ball
//
//  Created by bartekzabicki on 12.08.2015.
//  Copyright © 2015 bartekzabicki. All rights reserved.
//

import UIKit

class InfoController: UIViewController {
    @IBOutlet var infoView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let background = CAGradientLayer().violetColor()
        background.frame = self.infoView.frame
        background.frame.size.width *= 2
        self.infoView.layer.insertSublayer(background, atIndex: 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
