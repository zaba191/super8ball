//
//  AnswerView.swift
//  Super8Ball
//
//  Created by bartekzabicki on 06.08.2015.
//  Copyright © 2015 bartekzabicki. All rights reserved.
//

import UIKit

class AnswerView: UIView {
    @IBOutlet var answerLabel: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!

}
