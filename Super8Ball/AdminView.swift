//
//  AdminView.swift
//  Super8Ball
//
//  Created by bartekzabicki on 06.08.2015.
//  Copyright © 2015 bartekzabicki. All rights reserved.
//

import UIKit

class AdminView: UIView {

    @IBOutlet var textField: UITextField!
    @IBOutlet var addAnswerButton: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!


}
