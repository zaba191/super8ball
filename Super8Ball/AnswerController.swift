//
//  AnswerController.swift
//  Super8Ball
//
//  Created by bartekzabicki on 06.08.2015.
//  Copyright © 2015 bartekzabicki. All rights reserved.
//

import UIKit
import CloudKit
import MobileCoreServices
import WatchConnectivity
//import Realm

class AnswerController: UIViewController, WCSessionDelegate {
    @IBOutlet var answerView: AnswerView!
    
    let container = CKContainer.defaultContainer()
    var publicDatabase: CKDatabase?
    var currentRecord: CKRecord?
    var session: WCSession!
    let defaults = NSUserDefaults.standardUserDefaults()
    
    var positiveArray:[Answer] = []
    var neutralArray:[Answer] = []
    var negativeArray:[Answer] = []
    var fullArray:[Answer]!
    
    required init?(coder aDecoder: NSCoder) {
        self.session = WCSession.defaultSession()
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        answerView.answerLabel.hidden = true
        getLastUpdated()
        answerView.activityIndicator.startAnimating()
        publicDatabase = container.publicCloudDatabase
        downloadAnswers { (fullArray) -> Void in
            self.fullArray=fullArray
            self.setAnswer(self.getRandomAnswer())
        }
        
        if(WCSession.isSupported()) {
            session.delegate = self
            session.activateSession()
        }
        else{
            print("Session is not supported on iPhone")
        }
    }
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
        
        print(message)
        // Get random answer in main thread
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            print(message)
            print(self.getRandomAnswer)
        })
    }

    
    func downloadAnswers(completion: (([Answer]) -> Void)){
//        getLastUpdate()
        let predicate = NSPredicate(value: true)
        
        let query = CKQuery(recordType: "Answers", predicate: predicate)
        
        publicDatabase?.performQuery(query, inZoneWithID: nil,
            completionHandler: ({results, error in
                
                if (error != nil) {
                    dispatch_async(dispatch_get_main_queue()) {
                        //self.notifyUser("Cloud Access Error",
                            //message: error!.localizedDescription)
                    }
                } else {
                    if results!.count > 0 {
                        dispatch_async(dispatch_get_main_queue()) {
                            print("total rows: \(results!.count)")
                        }
                        for record in results! {
                            self.currentRecord = record
                            
                            dispatch_async(dispatch_get_main_queue()) {
                                let aLevelResult = record.objectForKey("answerLevel") as! Int
                                let answerResult = record.objectForKey("answer") as! String
                                let answerId = record.recordID
                                let answer = Answer(answerText: answerResult, answerLevel: aLevelResult, answerId: answerId)
                                switch(aLevelResult){
                                case 0:
                                    if !self.containAnswer(self.positiveArray, answer: answer){
                                        self.positiveArray.append(answer)
                                    }
                                    break
                                case 1:
                                    if !self.containAnswer(self.neutralArray, answer: answer){
                                        self.neutralArray.append(answer)
                                    }
                                    break
                                case 2:
                                    if !self.containAnswer(self.negativeArray, answer: answer){
                                        self.negativeArray.append(answer)
                                    }
                                    break
                                default:
                                    print("bad answerLevel")
                                }
                            }
                        }
                        dispatch_async(dispatch_get_main_queue()) {
                            completion(self.positiveArray+self.neutralArray + self.negativeArray)
                            self.answerView.activityIndicator.stopAnimating()
                            self.answerView.answerLabel.hidden = false
                        }

                    } else {
                        dispatch_async(dispatch_get_main_queue()) {
                            self.notifyUser("No Match Found",
                                message: "No record matching the address was found")
                        }
                    }
                }
            }))

    }
        //End of iCloud
        /*var targetArray:[String,] = [
            "It is certain",
            "It is decidedly so",
            "Without a doubt",
            "Yes definitely",
            "You may rely on it",
            "As I see it, yes",
            "Most likely",
            "Outlook good",
            "Yes",
            "Signs point to yes",
            "Reply hazy try again",
            "Ask again later",
            "Better not thell you know",
            "Cannot predict now",
            "Concentrate and ask again",
            "Don't count on it",
            "My reply is no",
            "My sources say no",
            "Outlook not so good",
            "Very doubtful",
            "No"
        ]*/
    
    func getRandomNumber(count:Int) -> Int{ //Array count
        var randomNumber:Int!
        randomNumber = Int(arc4random_uniform(UInt32(count)))
        return randomNumber
    }
    
    func getRandomAnswer() -> Answer {
        return fullArray[getRandomNumber(fullArray.count)]
    }
    
    func setAnswer(answer:Answer){
        let message = ["myAnswer" : answer]
        session.sendMessage(message, replyHandler: { (content:[String : AnyObject]) -> Void in
            print(message)
            print("Our counterpart sent something back. This is optional \(answer.text)")
            }, errorHandler: {  (error ) -> Void in
                print("We got an error from our watch device : " + error.domain)
        })
        
        answerView.answerLabel.text = answer.text
        let index = getIdByString(fullArray,item: answer.text)
        if index < positiveArray.count{
            //Positive answer
            answerView.answerLabel.textColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        }
        else if index >= positiveArray.count + neutralArray.count{
            //negative answer
            answerView.answerLabel.textColor = UIColor(red: 255/255, green: 45/255, blue: 85/255, alpha: 1)
        }
        else{
            //neutral answer
            answerView.answerLabel.textColor = UIColor(red: 52/255, green: 170/255, blue: 220/255, alpha: 1)
        }
    }
    override func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    override func motionEnded(motion: UIEventSubtype, withEvent event: UIEvent?) {
        if motion == .MotionShake{
            self.setAnswer(self.getRandomAnswer())
        }
    }
    
    func notifyUser(title: String, message: String) -> Void
    {
        let alert = UIAlertController(title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.Alert)
        
        let cancelAction = UIAlertAction(title: "OK",
            style: .Cancel, handler: nil)
        
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true,
            completion: nil)
    }
    
    func getIdByString(array:[Answer],item:String) -> Int{
        for var i=0;i<array.count;i+=1{
            if array[i].text == item{
                return i
            }
        }
        print("getIdByString failed")
        return 0
    }
    
    func getLastUpdated(){
        self.positiveArray = defaults.objectForKey("positiveArray") as! [Answer]
        self.neutralArray = defaults.objectForKey("neutralArray") as! [Answer]
        self.negativeArray = defaults.objectForKey("negativeArray") as! [Answer]
//        realmPositiveAnswers {
//            get {
//                let predicate = NSPredicate(format: "answer.level == 0", argumentArray: nil)
//                return RealmAnswer.objectsWithPredicate(predicate)
//            }
//        }
//        realmNeutralAnswers {
//            get {
//                let predicate = NSPredicate(format: "answer.level == 1", argumentArray: nil)
//                return RealmAnswer.objectsWithPredicate(predicate)
//            }
//        }
//        realmNegativeAnswers {
//            get {
//                let predicate = NSPredicate(format: "answer.level == 2", argumentArray: nil)
//                return RealmAnswer.objectsWithPredicate(predicate)
//            }
//        }
    }
    
    func containAnswer(tab:[Answer], answer:Answer) -> Bool{
        for element in tab{
            if element.text == answer.text{
                return true
            }
        }
        return false
    }

}
