//
//  MainView.swift
//  Super8Ball
//
//  Created by bartekzabicki on 06.08.2015.
//  Copyright © 2015 bartekzabicki. All rights reserved.
//

import UIKit

class MainView: UIView {
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet var adminButton: UIButton!
    @IBOutlet var infoButton: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!

}
