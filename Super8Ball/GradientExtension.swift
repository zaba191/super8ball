//
//  GradientExtension.swift
//  Super8Ball
//
//  Created by bartekzabicki on 14.08.2015.
//  Copyright © 2015 bartekzabicki. All rights reserved.
//

import UIKit

extension CAGradientLayer {
    
    func violetColor() -> CAGradientLayer {
        let topColor = UIColor(red: (0/255.0), green: (0/255.0), blue: (0/255.0), alpha: 1)
        let bottomColor = UIColor(red: (51/255.0), green: (0/255.0), blue: (102/255.0), alpha: 1)
        
        let gradientColors: [CGColor] = [topColor.CGColor, bottomColor.CGColor]
        let gradientLocations: [Float] = [0.0, 1.0]
        
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        gradientLayer.locations = gradientLocations
        
        return gradientLayer
    }
}
